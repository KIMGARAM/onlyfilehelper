package com.library.helper.onlyfilehelper;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;


/**
 * Created by focusone on 2017. 5. 19..
 */

public class OnlyFileHelper extends Activity{
    public OnlyFileHelper(){

    }

    // 디렉터리 만들기
    /**
     * Directory Make.
     *
     * @param TargetStorage         Storage location, PI or E or S
     * @param TargetFolderPath
     * @return
     */
    public boolean makeDirectory(int requestCode, String TargetStorage, String TargetFolderPath){
        boolean bool = false;

        if(!parameterStorageError(TargetStorage) || !parameterPathError(TargetFolderPath)){
            Log.i("ONLY", "The parameter is incorrect.");
            return false;
        }

        String targetStorageDirStr = TargetStorageSub(requestCode, TargetStorage, TargetFolderPath);

        if(targetStorageDirStr == null){
            //				Log.i("FILE", "External Not Mounted. && Not SDcard");
            bool = false;
            Log.i("ONLY", "External Not Mounted.");
        } else if (targetStorageDirStr.equals("P")) {
            bool = false;
            Log.i("ONLY", "Permission denied.");
        } else {
            File targetStorageDir = new File(targetStorageDirStr);
            if (targetStorageDir.exists() && targetStorageDir.isDirectory()) {
                bool = false;
                Log.i("ONLY", "The path already exists.");
            } else {
                targetStorageDir.mkdirs();
                bool = true;
            }
        }

        return bool;
    }

    // 디렉터리 명 수정
    public boolean renameDirectory(int requestCode, String TargetStorage, String TargetFolderPath, String AfterFolderPath){
        boolean bool = false;

        if(!parameterStorageError(TargetStorage) || !parameterPathError(TargetFolderPath) || !parameterPathError(AfterFolderPath)){
            Log.i("ONLY", "The parameter is incorrect.");
            return false;
        }

        String targetStorageDirStr = TargetStorageSub(requestCode, TargetStorage, TargetFolderPath);
        String newDirStr = TargetStorageSub(requestCode, TargetStorage, AfterFolderPath);

        if(targetStorageDirStr == null || newDirStr == null){
            //				Log.i("FILE", "External Not Mounted. && Not SDcard");
            bool = false;
            Log.i("ONLY", "External Not Mounted.");
        } else if (targetStorageDirStr.equals("P")) {
            bool = false;
            Log.i("ONLY", "Permission denied.");
        } else{
            File targetStorageDir = new File(targetStorageDirStr);
            File newDir = new File(newDirStr);

            if( !(targetStorageDir.exists()) || !(targetStorageDir.isDirectory()) ){
                // 기존경로가 없을 경우.
                bool = false;
                Log.i("ONLY", "The path does not exist.");
            } else if (newDir.exists() && targetStorageDir.isDirectory()) {
                bool = false;
                Log.i("ONLY", "The path already exists.");
            } else{
                if(targetStorageDir.renameTo(newDir)){
                    //					Log.i("FILE", "디렉터리 변경 성공.");
                    bool = true;
                }else{
                    //					Log.i("FILE", "디렉터리 변경 실패.");
                    bool = false;
                    Log.i("ONLY", "The path already exists.");
                }
            }
        }


        return bool;
    }

    // 디렉터리 삭제
    public boolean deleteDirectory(int requestCode, String TargetStorage, String TargetFolderPath){
        boolean bool = false;

        if(!parameterStorageError(TargetStorage) || !parameterPathError(TargetFolderPath)){
            Log.i("ONLY", "The parameter is incorrect.");
            return false;
        }

        String targetStorageDirStr = TargetStorageSub(requestCode, TargetStorage, TargetFolderPath);

        if(targetStorageDirStr == null){
            //				Log.i("FILE", "External Not Mounted. && Not SDcard");
            bool = false;
            Log.i("ONLY", "External Not Mounted.");
        } else if (targetStorageDirStr.equals("P")) {
            bool = false;
            Log.i("ONLY", "Permission denied.");
        } else{
            File targetStorageDir = new File(targetStorageDirStr);
            if(!targetStorageDir.exists() || !targetStorageDir.isDirectory()){
                bool = false;
                Log.i("ONLY", "The path does not exist.");
            } else {
                if(deleteDirectorySubFiles(TargetStorage, targetStorageDir)){
                    targetStorageDir.delete();
                    bool = true;
                } else{
                    bool = false;
                    Log.i("ONLY", "Errors while reading and writing files.");
                }
            }
        }


        return bool;
    }

    // 디렉터리 내부 삭제
    boolean deleteDirectorySubFiles(String TargetStorage, File dir){
        // 디렉터리 내부 파일 전체 삭제하는 함수. 디렉터리 삭제는 모든 파일 삭제한 후에 삭제한다.
        boolean bool = false;

        try {
            if(dir.isDirectory() && dir.listFiles() == null){
                // 디렉토리이며 리스트를 가지고 있지 않을 경우
                dir.delete();
                return true;
            }else{
                for (File f : dir.listFiles()) {
                    if (f.isFile()) {
                        if (f.exists()) {
                            Log.i("ONLY", f.getAbsolutePath() + " : 디렉터리 내부 파일 삭제");

                            deleteFileSub(TargetStorage, f);
                            // =========================
                            // 폴더 통째로 지우면 파일이 안에 있을 경우 예외처리 난다. 안에 아무것도 없을 경우는 정상적으로 삭제 됨.
                        }
                    } else {
                        // 내부에 폴더가 있을 경우.
                        deleteDirectorySubFiles(TargetStorage, f);
                    }
                }
                dir.delete();
                bool = true;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            bool = false;
        }
        return bool;
    }

    // 파일 생성
    public boolean createFile(int requestCode, String TargetStorage, String TargetFilePath, String FileData){
        boolean bool = false;

        if(!parameterStorageError(TargetStorage) || !parameterFilePathError(TargetFilePath) || !parameterNullCheckError(FileData)){
            Log.i("ONLY", "The parameter is incorrect.");
            return false;
        }

        String targetStorageFileStr = TargetStorageSub(requestCode, TargetStorage, TargetFilePath);

        if(targetStorageFileStr == null){
            //				Log.i("FILE", "External Not Mounted. && Not SDcard");
            bool = false;
            Log.i("ONLY", "External Not Mounted.");
        } else if (targetStorageFileStr.equals("P")) {
            bool = false;
            Log.i("ONLY", "Permission denied.");
        } else{
            // 디렉터리가 존재하는지 아닌지 확인. 존재하지 않다면 false.
            File targetStorageFile = new File(targetStorageFileStr);
            if(targetStorageFile.exists() && targetStorageFile.isFile()){
                bool = false;
                Log.i("ONLY", "The path already exists.");
            } else{
                // 파일이 존재하지 않는다.
                bool = createFileSub(targetStorageFile, FileData);	// 상위폴더와 파일을 생성해줌.

                if(bool == false){
                    Log.i("ONLY", "Errors while reading and writing files.");
                }
            }
        }


        return bool;
    }

    // 파일 생성 보조
    /**
     *
     * @param file				Create file
     * @param FileData			New file data
     * @return
     */
    private boolean createFileSub(File file, String FileData){
        boolean bool = false;
        try {
            // 파일 만들기.
            File newFile = new File(file.getAbsolutePath());
            File newDir = new File(newFile.getParent());
            if(!newDir.exists() || !newDir.isDirectory()){
                newDir.mkdirs();
            }
            newFile.createNewFile();

            if(newFile.exists()){
                if(newFile.isFile()){
                    byte[] data = Base64.decode(FileData, Base64.DEFAULT);

                    FileOutputStream fileOutputStream = new FileOutputStream(newFile);
                    fileOutputStream.write(data);
                    fileOutputStream.close();

                    MediaScanning mediaScanning = new MediaScanning(OnlyFileHelper.this, newFile);

                    bool = true;
                }
            } else{
                // 파일 생성 실패
                bool = false;
            }

        } catch (IOException e){
            e.printStackTrace();
            bool = false;

        } catch (Exception eAll) {
            // TODO Auto-generated catch block
            eAll.printStackTrace();
            bool = false;

        }

        return bool;
    }


    // 파일 읽기
    public String readFile(int requestCode, String TargetStorage, String TargetFilePath){
        String str = "";

        if(!parameterStorageError(TargetStorage) || !parameterFilePathError(TargetFilePath)){
            Log.i("ONLY", "The parameter is incorrect.");
            return "";
        }

        String targetStorageFileStr = TargetStorageSub(requestCode, TargetStorage, TargetFilePath);

        if(targetStorageFileStr == null){
            Log.i("ONLY", "External Not Mounted.");
        } else if (targetStorageFileStr.equals("P")) {
            str = "";
            Log.i("ONLY", "Permission denied.");
        } else{
            File targetStorageFile = new File(targetStorageFileStr);

            if(targetStorageFile.exists()){
                if(targetStorageFile.isFile()){
                    try {
                        // BufferedInputStream이 버퍼의 데이터를 바이트로 읽어온다.
                        BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(targetStorageFile));
                        byte[] data = new byte[(int) targetStorageFile.length()];
                        bufferedInputStream.read(data, 0, data.length);
                        bufferedInputStream.close();

                        // byte를 복호화
//					data = new SecretFileAES().decodeFile("TheBestSecretKey", data);

                        String base64 = Base64.encodeToString(data, Base64.DEFAULT);	// string에 base64로 인코딩한 데이터를 담는다.
                        str = base64;
                    } catch (IOException e) {
                        e.printStackTrace();
                        str = "";
                    }
                }
            }
        }


        return str;
    }


    // 파일 수정
    public boolean editFile(int requestCode, String TargetStorage, String TargetFilePath, String FileData){
        boolean bool = false;

        if(!parameterStorageError(TargetStorage) || !parameterFilePathError(TargetFilePath) || !parameterNullCheckError(FileData)){
            Log.i("ONLY", "The parameter is incorrect.");
            return false;
        }

        String targetStorageFileStr = TargetStorageSub(requestCode, TargetStorage, TargetFilePath);

        if(targetStorageFileStr == null){
            //				Log.i("FILE", "External Not Mounted. && Not SDcard");
            bool = false;
            Log.i("ONLY", "External Not Mounted.");
        } else if (targetStorageFileStr.equals("P")) {
            bool = false;
            Log.i("ONLY", "Permission denied.");
        } else{
            File targetStorageFile = new File(targetStorageFileStr);

            if(targetStorageFile.exists() && targetStorageFile.isFile()){
                if(deleteFileSub(TargetStorage, targetStorageFile)){
                    bool = createFileSub(targetStorageFile, FileData);

                    if(bool == false){
                        Log.i("ONLY", "Errors while reading and writing files.");
                    }
                }else{
                    //					Log.i("FILE", "삭제 실패  bool : "+bool);
                    bool = false;
                    Log.i("ONLY", "Errors while reading and writing files.");
                }
            } else{
                // file 이 존재하지않을 경우
                bool = false;
                Log.i("ONLY", "The path does not exist.");
            }
        }
        return bool;
    }

    // 파일 복사
    /**
     *
     * @param TargetStorage			Storage location, I or E or S or A
     * @param TargetFilePath		Existing file path (The file must exist in the file path. False if not)
     * @param newTargetStorage		Storage location to be copied, I or E or S or A
     * @param newTargetFilePath		File Path to be copied.
     * @return
     */
    public boolean copyFile(int requestCode, String TargetStorage, String TargetFilePath, String newTargetStorage, String newTargetFilePath)
    {
        boolean bool = false;
        byte[] tempData;

        if(!parameterStorageError(TargetStorage) || !parameterFilePathError(TargetFilePath) || !parameterStorageError(newTargetStorage) || !parameterFilePathError(newTargetFilePath)){
            Log.i("ONLY", "The parameter is incorrect.");
            return false;
        }

        // 파일 복사 경로와 기존 같은 파일 경로를 가진 파일이 있을 경우 false 리턴
        String targetStorageFileStr = TargetStorageSub(requestCode, TargetStorage, TargetFilePath);
        String targetStoragenewFileStr = TargetStorageSub(requestCode, newTargetStorage, newTargetFilePath);

        if(targetStoragenewFileStr == null || targetStorageFileStr == null){
            // 복사 파일 경로가 존재하지 않을 경우.
            Log.i("ONLY", "External Not Mounted");
            return false;
        }

        if (targetStorageFileStr.equals("P") || targetStoragenewFileStr.equals("P")) {
            Log.i("ONLY", "Permission denied.");
            return false;
        }

        // 복사 경로가 존재할 때
        File targetStorageFile	  = new File(targetStorageFileStr);
        File targetStoragenewFile = new File(targetStoragenewFileStr);

        if(!targetStorageFile.exists() || !targetStorageFile.isFile()){
            // 복사 해야할 경로가 없을 경우
            Log.i("ONLY", "The path does not exist.");
            return false;
        }
        if(targetStoragenewFile.exists() && targetStoragenewFile.isFile()){
            // 복사 경로가 존재할 때
            Log.i("ONLY", "The path already exists.");
            return false;
        }

        try {
            InputStream inputStream = new FileInputStream(targetStorageFile);
            int fileSize = inputStream.available();
            tempData = new byte[fileSize];
            inputStream.read(tempData);

            File newDir = new File(targetStoragenewFile.getParent());
            if(!newDir.exists() || !newDir.isDirectory()){
                newDir.mkdirs();
            }
            targetStoragenewFile.createNewFile();

            FileOutputStream fileOutputStream = new FileOutputStream(targetStoragenewFile);
            fileOutputStream.write(tempData);

            inputStream.close();
            fileOutputStream.close();

            MediaScanning mediaScanning = new MediaScanning(OnlyFileHelper.this, targetStoragenewFile);

            bool = true;
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            bool = false;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            bool = false;
        }

        return bool;
    }

    // 파일 삭제
    public boolean deleteFile(int requestCode, String TargetStorage, String TargetFilePath){
        boolean bool = false;

        if(!parameterStorageError(TargetStorage) || !parameterFilePathError(TargetFilePath)){
            Log.i("ONLY", "The parameter is incorrect.");
            return false;
        }

        String targetStorageFileStr = TargetStorageSub(requestCode, TargetStorage, TargetFilePath);

        if(targetStorageFileStr == null){
            Log.i("ONLY", "External Not Mounted.");
            return false;
        }

        if (targetStorageFileStr.equals("P")) {
            Log.i("ONLY", "Permission denied.");
            return false;
        }

        File targetStorageFile = new File(targetStorageFileStr);
        if(!targetStorageFile.exists() || !targetStorageFile.isFile()){
            // 존재하지 않을 경우
            Log.i("ONLY", "The path does not exist.");
            return false;
        }

        // 파일이 존재할 경우. 삭제 작업.
        bool = deleteFileSub(TargetStorage, targetStorageFile);

        return bool;
    }

    // 파일 삭제 보조
    private boolean deleteFileSub(String TargetStorage, File file){
        boolean bool = false;
        try {

            file.delete();

            if(file.exists()){
                Log.i("ONLY", TargetStorage + " : 파일삭제실패...");
                bool = false;
            } else{
                Log.i("ONLY", TargetStorage + " : 파일삭제성공...");
                bool = true;
            }

        } catch (Exception eAll) {
            // TODO Auto-generated catch block
            eAll.printStackTrace();
            bool = false;
        }

        return bool;
    }

    // 파일 목록 가져오기
    public JSONArray getFileList(int requestCode, String TargetStorage, String TargetFolderPath){
        JSONArray jsonArray = new JSONArray();
        String targetStorageDirStr = TargetStorageSub(requestCode, TargetStorage, TargetFolderPath);

        if(targetStorageDirStr == null){
            Log.i("ONLY", "External Not Mounted.");

            // json.put(); -> null말고 빈값.
            try {
                JSONObject json     = new JSONObject();
                json.put("FileLists", "");
                jsonArray.put(json);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (Exception eAll){
                eAll.printStackTrace();
            }
            return jsonArray;
        }

        if (targetStorageDirStr.equals("P")) {
            Log.i("ONLY", "Permission denied.");

            try {
                JSONObject json     = new JSONObject();
                json.put("FileLists", "");
                jsonArray.put(json);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (Exception eAll){
                eAll.printStackTrace();
            }
            return jsonArray;
        }

        File targetStorageDir = new File(targetStorageDirStr);

        try {
            FileFilter fileListFilter = new FileFilter(){
                @Override
                public boolean accept(File pathname) {
                    // TODO Auto-generated method stub
                    // 파일 목록 중 파일인 것만 필터처리.
                    return pathname.isFile();
                }

            };

            if(targetStorageDir.exists()){
                if(targetStorageDir.isDirectory()){
                    // 경로가 존재하고 디렉토리일 경우
                    for(File f : targetStorageDir.listFiles(fileListFilter)){
                        JSONObject json = new JSONObject();

                        // 리턴
                        //json.put("FileLists", file.listFiles(fileListFilter));
                        json.put("FileLists", f.getAbsolutePath());
                        jsonArray.put(json);
                    }
                }
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

            try {
                JSONObject json = new JSONObject();
                json.put("FileLists", "");
            } catch (JSONException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        } catch (Exception eAll) {
            // TODO Auto-generated catch block
            eAll.printStackTrace();

            try {
                JSONObject json = new JSONObject();
                json.put("FileLists", "");
            } catch (JSONException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }


        return jsonArray;
    }

    // 파일 명 검색
    public JSONArray searchFileName(int requestCode, String TargetStorage, String TargetFilePath, final String SearchKeyword){
        JSONArray jsonArray = new JSONArray();

        String targetStorageDirStr = TargetStorageSub(requestCode, TargetStorage, TargetFilePath);

        if(targetStorageDirStr == null){
            Log.i("ONLY", "External Not Mounted.");

            // json.put(); -> null말고 빈값.
            try {
                JSONObject json     = new JSONObject();
                json.put("FileLists", "");
                jsonArray.put(json);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (Exception eAll){
                eAll.printStackTrace();
            }

            return jsonArray;

        } else if (targetStorageDirStr.equals("P")) {
            Log.i("ONLY", "Permission denied.");

            try {
                JSONObject json     = new JSONObject();
                json.put("FileLists", "");
                jsonArray.put(json);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (Exception eAll){
                eAll.printStackTrace();
            }

            return jsonArray;
        } else{
            File targetStorageDir = new File(targetStorageDirStr);

            try {
                FilenameFilter fileNameFilter = new FilenameFilter(){

                    @Override
                    public boolean accept(File dir, String filename) {
                        // TODO Auto-generated method stub
                        return filename.contains(SearchKeyword);
                    }
                };

                if(targetStorageDir.exists()){
                    if(targetStorageDir.isDirectory()){
                        // 경로가 존재하고 검색어를 포함하고 있는 파일 일 경우
                        for(File f : targetStorageDir.listFiles(fileNameFilter)){
                            if(f.isFile()){
//                                Log.i("FILE", TargetStorage + " : 파일 리스트! : " + f.getAbsolutePath());

                                // 리턴
                                JSONObject json = new JSONObject();

                                json.put("FileLists", f.getAbsolutePath());
                                jsonArray.put(json);
                            }
                        }
                    }
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

                try {
                    JSONObject json = new JSONObject();
                    json.put("FileLists", "");
                    jsonArray.put(json);
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            } catch (Exception eAll) {
                // TODO Auto-generated catch block
                eAll.printStackTrace();

                try {
                    JSONObject json = new JSONObject();
                    json.put("FileLists", "");
                    jsonArray.put(json);
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        }

        return jsonArray;
    }

    // 파일 서버에 업로드
    public boolean uploadFile(int requestCode, String UploadUrlPath, String TargetStorage, String TargetFilePath){
        boolean bool = false;

        String targetStorageFileStr = TargetStorageSub(requestCode, TargetStorage, TargetFilePath);

        if(targetStorageFileStr == null){
            Log.i("ONLY", "External Not Mounted.");
            return false;
        }else if (targetStorageFileStr.equals("P")) {
            Log.i("ONLY", "Permission denied.");
            return false;
        } else{
            // 메인 쓰레드에서 네트워크 관련 처리를 할 경우 NetworkOnMainThreadException 에러 : 어플 다운.
            // 쓰레드를 생성해서 처리해줘야함.
            File targetStorageFile = new File(targetStorageFileStr);

            if(targetStorageFile.exists()){
                bool = uploadFileSub(UploadUrlPath, targetStorageFile);
            } else{
                // 업로드할 파일이 존재하지 않는다.
            }
        }

        return bool;
    }

    boolean tempBool = false;
    boolean uploadFileSub(final String UploadUrlPath, final File targetStorageFile){
        tempBool = false;

        Thread H1FileUploadThread = new Thread(new Runnable(){

            @Override
            public void run() {
                // TODO Auto-generated method stub

                try {
                    MultipartUtility multipart = new MultipartUtility(UploadUrlPath, "UTF-8");

                    // 암호화 하는 메소드 : addFilePart -> addFilePartAES
                    multipart.addFilePart("attachment", targetStorageFile);

                    multipart.finish();
                    tempBool = true;
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    tempBool = false;
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });

        H1FileUploadThread.start();
        try {
            H1FileUploadThread.join();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            tempBool = false;
        }
        return tempBool;
    }

    // url에 있는 파일 다운로드
    public boolean downloadFile(int requestCode, String DownloadUrlPath, String TargetStorage, String TargetFilePath){
        boolean bool = false;

        String targetStoragenewFileStr = TargetStorageSub(requestCode, TargetStorage, TargetFilePath);

        if(targetStoragenewFileStr == null){
            Log.i("ONLY", "External Not Mounted.");
            return false;
        } else if (targetStoragenewFileStr.equals("P")) {
            Log.i("ONLY", "Permission denied.");
            return false;
        } else{
            // 메인 쓰레드에서 네트워크 관련 처리를 할 경우 NetworkOnMainThreadException 에러 : 어플 다운.
            // 쓰레드를 생성해서 처리해줘야함.

            File targetStoragenewFile = new File(targetStoragenewFileStr);
            bool = H1FileDownloadSub(DownloadUrlPath, targetStoragenewFile);
        }

        return bool;
    }

    /**
     *
     * @param DownloadUrlPath					Url with file to download.
     * @param targetStoragenewFile		File path to save downloaded file.
     * @return
     */
    boolean H1FileDownloadSub(final String DownloadUrlPath, final File targetStoragenewFile){
        tempBool = false;
        Thread H1FileDownloadThead = new Thread(new Runnable(){

            @Override
            public void run() {
                // TODO Auto-generated method stub
                HttpURLConnection httpConn;
                try {
                    URL url = new URL(DownloadUrlPath);
                    httpConn = (HttpURLConnection) url.openConnection();
                    int responseCode = httpConn.getResponseCode();

                    // always check HTTP response code first
                    if (responseCode == HttpURLConnection.HTTP_OK) {
                        String fileName = "";
                        String disposition = httpConn.getHeaderField("Content-Disposition");
                        String contentType = httpConn.getContentType();
                        int contentLength = httpConn.getContentLength();

                        if (disposition != null) {
                            // extracts file name from header field
                            int index = disposition.indexOf("filename=");
                            if (index > 0) {
                                fileName = disposition.substring(index + 10,
                                        disposition.length() - 1);
                            }
                        } else {
                            // extracts file name from URL
                            fileName = DownloadUrlPath.substring(DownloadUrlPath.lastIndexOf("/") + 1,
                                    DownloadUrlPath.length());
                        }

                        Log.i("FILE", "Content-Type = " + contentType);
                        Log.i("FILE", "Content-Disposition = " + disposition);
                        Log.i("FILE", "Content-Length = " + contentLength);
                        Log.i("FILE", "fileName = " + fileName);

                        InputStream inputStream = httpConn.getInputStream();

                        String saveFilePath = targetStoragenewFile.getAbsolutePath();

                        File newDir = new File(targetStoragenewFile.getParent());
                        if(!newDir.exists() || !newDir.isDirectory()){
                            newDir.mkdirs();
                        }

                        // opens an output stream to save into file
                        FileOutputStream outputStream = new FileOutputStream(saveFilePath);

                        try {
                            byte[] data = MultipartUtility.inputStreamToByteArray(inputStream);

                            // data 암호화
//							data = new SecretFileAES().encodeFile("TheBestSecretKey", data);

                            outputStream.write(data);
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                        outputStream.close();
                        inputStream.close();

                        MediaScanning mediaScanning = new MediaScanning(OnlyFileHelper.this, targetStoragenewFile);

                        Log.i("FILE", "File downloaded");
                        tempBool = true;
                    } else {
                        System.out.println("No file to download. Server replied HTTP code: " + responseCode);
                        tempBool = false;
                    }
                    httpConn.disconnect();

                } catch (MalformedURLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    tempBool = false;
                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    tempBool = false;
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    tempBool = false;
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    tempBool = false;
                }
            }

        });

        H1FileDownloadThead.start();
        try {
            H1FileDownloadThead.join();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            tempBool = false;
        }

        return tempBool;
    }


    // 파일 경로 뽑기
    /**
     *
     * @param requestCode
     * @param TargetStorage         Storage location, I or E or S or A
     * @param TargetPath            The path to the file sent by JavaScript. (Data about the file path users want)
     * @return
     */
    String TargetStorageSub(int requestCode, String TargetStorage, String TargetPath){
        String return_str = null;
        File[] files;
        int sdk_int = Build.VERSION.SDK_INT;

        if(!(new SubHelper(this).checkPermissionExternal(requestCode))){
            return "P";
        }

        if(TargetPath == null) TargetPath = "";

        // 내부, 외부, sd카드 구별
        if(TargetStorage.equals("A")){
            // 내부
            String path = this.getDir("www", Context.MODE_PRIVATE).getAbsolutePath();

            return_str = path + "/" + TargetPath;
        } else if(TargetStorage.equals("PI")){
            // 내부
            String path = this.getFilesDir().getAbsolutePath();

            return_str = path + TargetPath;
        } else if(TargetStorage.equals("E")){
            // 외장 메모리 사용 가능 체크
            if( !(new SubHelper().isExternalStorageWritable())){
                return null;
            }

            return_str = Environment.getExternalStorageDirectory().getAbsolutePath() +TargetPath;
        }
        // 내부일 경우, sd카드일 경우 -> 앱 자체 폴더 안에 !!
        else if(TargetStorage.equals("I") || TargetStorage.equals("S")){
            // 버전 체크
            // JELLY_BEAN : 16, JELLY_BEAN_MR2 : 18
            // 외장 메모리 사용 가능 체크
            if( !(new SubHelper().isExternalStorageWritable())){
                Log.i("ONLY", "State false && External Not Mounted.");
                return null;
            }

            if (sdk_int < Build.VERSION_CODES.JELLY_BEAN) {
                //단말기 OS버전이 젤라빈 버전 보다 작을때.....처리 코드
                HashSet<String> hashSet = new SubHelper().getExternalMounts();
                int hashSetSize = hashSet.size();
                Log.i("ONLY", TargetStorage + " : hashSet : " + hashSet);
                // [/mnt/sdcard/external_sd, /mnt/sdcard]
                Log.i("ONLY", TargetStorage + " : hashSetSize : " + hashSetSize);

                // 배열 초기화
                files = new File[hashSetSize];

                int i = hashSetSize -1;
                for(String hashData : hashSet){
                    // 해시 데이타 확인
                    Log.i("ONLY", TargetStorage + " : hashData : " + hashData);
                    String packageName = this.getPackageName();

                    files[i] = new File(hashData+"/Android/data/"+packageName+"/files");
                    Log.i("ONLY", TargetStorage + " : file ["  + i + "] : " + files[i].getAbsolutePath());
                    i --;
                }

            } else if(sdk_int >= Build.VERSION_CODES.JELLY_BEAN && sdk_int <= Build.VERSION_CODES.JELLY_BEAN_MR2){
                //단말기 OS버전이 젤라빈 버전 보다 작을때.....처리 코드
                HashSet<String> hashSet = new SubHelper().getExternalMounts();
                int hashSetSize = hashSet.size();
                // [/mnt/sdcard/external_sd]

                // 배열 초기화
                files = new File[hashSetSize+1];
                String packageName = this.getPackageName();

                //files[0] = Environment.getExternalStorageDirectory()+"/Android/data/"+packageName+"/files";
                files[0] = new File(Environment.getExternalStorageDirectory()+"/Android/data/"+packageName+"/files");
                Log.i("ONLY", TargetStorage + " : file [0] : " + files[0].getAbsolutePath());

                if(hashSetSize == 1){
                    files[1] = new File(hashSet.iterator().next()+"/Android/data/"+packageName+"/files");
                    Log.i("ONLY", TargetStorage + " : file [1] : " + files[1].getAbsolutePath());
                }
            } 
            else{
                // 젤라빈 버전 이상일때.....처리 코드
                File[] filesTemp = new File[0];
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                    filesTemp = this.getExternalFilesDirs(null);
                }

                if(filesTemp[0] == null){
                    filesTemp[0] = new File(Environment.getExternalStorageDirectory()+"/Android/data/"+this.getPackageName()+"/files");
                }

                // 배열 초기화
                files = new File[filesTemp.length];

                Log.i("ONLY", "filesTemp.length : " + filesTemp.length);
                Log.i("ONLY", "filesTemp : " + filesTemp);

                // file
                if(files.length<2 || filesTemp[1] == null){
                    //String path = filesTemp[0].getPath().split("/Android")[0];
                    String path = filesTemp[0].getAbsolutePath();
                    Log.i("ONLY", "file[1 == null] path : " + path);

                    files[0] = new File(path);

                }else{
                    int i = 0;
                    for(File file : filesTemp){
                        // /storage/emulated/0
                        // /storage/A2CA-18E4
                        files[i] = new File(file.getAbsolutePath());
                        i ++;
                    }
                }
            }

            // 파일 목록 뽑아 주기.
            if(TargetStorage.equals("I")){
                return_str = files[0] + TargetPath;
            }else if(TargetStorage.equals("S")){
                if(files.length<2 || files[1] == null){
                    Log.i("ONLY", "SD card가 존재하지않습니다.");
                    return_str = null;
                }else{
                    return_str = files[1] + TargetPath;
                }
            }

        }

        Log.i("ONLY", "TargetStorageSub Return File path : " + return_str);

        return return_str;
    }

    private boolean parameterNullCheckError(String checkStr){
        if(checkStr.equals("") || checkStr == null)
            return false;
        else return true;
    }


    private boolean parameterStorageError(String storage){
        if(!parameterNullCheckError(storage)) return false; 	// storage가 빈값일 경우

        if(storage.equals("PI") || storage.equals("I") || storage.equals("E") ||storage.equals("S")) return true;
        else return false;
    }

    private boolean parameterPathError(String dirPath){
        if(!parameterNullCheckError(dirPath)) return false; 	// storage가 빈값일 경우

        String firstStr = dirPath.substring(0, 1);
        if(firstStr.equals("/")) return true;
        else return false;
    }

    private boolean parameterFilePathError(String filePath){
        if(!parameterPathError(filePath)) return false;	// 첫 글자가 /가 아닌 경우 false or filePath가 빈 값인 경우
        int index = filePath.indexOf(".");
        if(index<0) return false;	// . string을 가지지 않고 있다. -1
        else return true;			// . string을 가지고 있다.
    }

}
