package com.library.helper.onlyfilehelper;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

import java.io.InputStream;
import java.util.HashSet;
import java.util.Locale;

/**
 * Created by focusone on 2017. 5. 19..
 */

public class SubHelper {
    Activity act;

    public SubHelper(){}
    public SubHelper(Activity activity){
        act = activity;
    }

    // [파일 명 가져오기] ===============================================================================
    public String[] getFileName(String fileStr){
        String[] filePath = new String[2];
        int index = fileStr.lastIndexOf("/");
        if(index >= 0) {
            filePath[0] = fileStr.substring(0, index);
            filePath[1] = fileStr.substring(index+1, fileStr.length());
        }
        return filePath;
    }

    // [외장메모리 저장 가능여부 체크] ====================================================================
    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    // [하위 버전 마운트 저장소 구하기] ====================================================================
    public static HashSet<String> getExternalMounts() {
        final HashSet<String> out = new HashSet<String>();
        String reg = "(?i).*vold.*(vfat|ntfs|exfat|fat32|ext3|ext4).*rw.*";
        String s = "";

        try {
            final Process process = new ProcessBuilder().command("mount")
                    .redirectErrorStream(true).start();
            process.waitFor();
            final InputStream is = process.getInputStream();
            final byte[] buffer = new byte[1024];
            while (is.read(buffer) != -1) {
                s = s + new String(buffer);
            }
            is.close();
        } catch (final Exception e) {
            e.printStackTrace();
        }

        // parse output
        final String[] lines = s.split("\n");
        for (String line : lines) {
            if (!line.toLowerCase(Locale.US).contains("asec")) {
                if (line.matches(reg)) {
                    String[] parts = line.split(" ");
                    for (String part : parts) {
                        if (part.startsWith("/"))
                            if (!part.toLowerCase(Locale.US).contains("vold"))
                                out.add(part);
                    }
                }
            }
        }
        return out;
    }

    // [외장메모리 퍼미션 체크] ====================================================================
    @TargetApi(Build.VERSION_CODES.M)
    public boolean checkPermissionExternal(int requestCode) {
        int sdk_int = Build.VERSION.SDK_INT;

        if(sdk_int >= Build.VERSION_CODES.M){
            Log.i("FILE", "Build.VERSION_CODES.M : " + Build.VERSION_CODES.M);
            if (act.checkSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE")
                    != PackageManager.PERMISSION_GRANTED
                    || act.checkSelfPermission("android.permission.READ_EXTERNAL_STORAGE")
                    != PackageManager.PERMISSION_GRANTED) {
                Log.i("PERMISSION", "checkPermission if " );

                //            // Should we show an explanation?
                //            // 이 권한을 필요한 이유를 설명할 것
                if (act.shouldShowRequestPermissionRationale("android.permission.WRITE_EXTERNAL_STORAGE")) {
                    // Explain to the user why we need to write the permission.
                    // 다이얼로그 같은 것을 띄워 필요한 이유 설명
                    // 해당 설명이 끝난 뒤 함수 호출 하여 권한 허가 요청
                    act.requestPermissions(new String[]{"android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"},
                            requestCode);
                    // 사용자가 권한을 거절하면 shouldShowRequestPermissionRationale true

                }else {
                    act.requestPermissions(new String[]{"android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"},
                            requestCode);
                }

                return false;

            } else {
                // 다음 부분은 항상 허용일 경우에 해당이 됩니다.
                Log.i("FILE", "checkPermission OK! " );
                return true;
            }
        } else{
            return true;
        }
    }


}
